import React from 'react';

import {
    Dimmer,
    Loader,
    Modal,
    Form,
    TextArea,
} from 'semantic-ui-react';

export default ({ data, isOpen, onClose }) => (
    <Modal
        open={ isOpen }
        onClose={ onClose }>
        <Modal.Header>Here is what you ordered</Modal.Header>
        <Modal.Content>
            {
                data.loading === true &&
                <Dimmer inverted active>
                    <Loader inverted>Exporting...</Loader>
                </Dimmer>
            }
            <Form>
                <TextArea
                    value={ data.data }
                    style={{ height: 350 }}
                />
            </Form>
        </Modal.Content>
        <Modal.Actions
            actions={[
                { key: 'no', content: 'No', color: 'red', triggerClose: true },
                { key: 'yes', content: 'Yes', color: 'green', triggerClose: true },
            ]} />
    </Modal>
);
