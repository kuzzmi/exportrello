import React from 'react';

import {
    Modal,
} from 'semantic-ui-react';

export default ({ isOpen, onClose }) => (
    <Modal
        open={ isOpen }
        onClose={ onClose }>
        <Modal.Header>Exportrello needs your support</Modal.Header>
        <Modal.Content>
            <p>Please consider donating some bitcoins to my donation wallet: <a href="bitcoin:15nip1T8PuhLi1geep17M4vTUdHzpCY4H6">15nip1T8PuhLi1geep17M4vTUdHzpCY4H6</a></p>
        </Modal.Content>
        <Modal.Actions
            actions={[
                { key: 'no', content: 'No', color: 'red', triggerClose: true },
                { key: 'yes', content: 'Yes', color: 'green', triggerClose: true },
            ]} />
    </Modal>
);

