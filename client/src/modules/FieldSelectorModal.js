import React from 'react';

import {
    Dimmer,
    Loader,
    Modal,
    Checkbox,
    Button,
} from 'semantic-ui-react';

const FIELD_TO_STRING = {
    id: 'ID of a card',
    checkItemStates: 'Checklist items states',
    closed: 'Is a card closed?',
    dateLastActivity: 'Last activity date',
    desc: 'Description',
    descData: 'Description data (e.g. emojies)',
    idBoard: 'ID of a board',
    idList: 'ID of a list',
    idMembersVoted: 'IDs of members who voted for this card',
    idShort: 'Numeric ID for the card on the board, subject to change',
    idAttachmentCover: 'ID of an attachment used as a cover for the card',
    idLabels: 'IDs of labels used',
    manualCoverAttachment: 'Is attachment cover was set manually?',
    name: 'Name of a card',
    pos: 'Position in the list',
    shortLink: 'Shortened link to the card',
    badges: 'Badges used on the card',
    dueComplete: 'Is due date was marked as completed?',
    due: 'The due date of the card',
    idChecklists: 'IDs of checklists',
    idMembers: 'IDs of assigned members',
    labels: 'Array of labels used',
    shortUrl: 'URL to a card without its name',
    subscribed: 'Are you subscribed?',
    url: 'Full URL to a card',
    pluginData: 'Data stored by add-ons',
    exportedOn: 'When the export was made',
    listName: 'A name of the list',
};

export default ({ data, isOpen, onClose, onExportClick, onFieldToggle }) => (
    <Modal
        open={ isOpen }
        onClose={ onClose }>
        <Modal.Header>Before you proceed with your export, please select fields you want to extract</Modal.Header>
        <Modal.Content>
            {
                data.loading === true ?
                    <Dimmer inverted active>
                        <Loader inverted>Extracting fields...</Loader>
                    </Dimmer> :
                    <div>
                        <p>We will save what you selected here for this board after you make the export.</p>
                        <p>
                            For better explanation on what all these fields actually mean, please visit Trello documentation <a href="https://developers.trello.com/v1.0/reference#card-object">here</a>.
                        </p>
                        <div style={{ height: 10 }} />
                    </div>
            }
            {
                data.data !== false &&
                data.data.fields.map(( field, index ) => (
                    <div>
                        <Checkbox
                            key={ index }
                            checked={ data.data.selected.indexOf(field) > -1 }
                            label={
                                <label><b>{field}</b>: {FIELD_TO_STRING[field] || <i>custom field</i>}</label>
                            }
                            onChange={ () => onFieldToggle({ field }) }
                        />
                    </div>
                ))
            }
        </Modal.Content>
        <Modal.Actions
            actions={[
                <Button color="grey" content="Cancel" key="cancel" onClick={ onClose } />,
                <Button positive icon="checkmark" key="export" labelPosition="right" content="Export" onClick={ () => onExportClick(data.board.id, 'csv') } />,
            ]} />
    </Modal>
);

