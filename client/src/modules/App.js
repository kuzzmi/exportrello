import React, { Component } from 'react';
import Box, { Container } from 'react-layout-components';

// Routing
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Switch,
} from 'react-router-dom';

import {
    Segment,
} from 'semantic-ui-react';

import Navbar from './Navbar.js';
import BoardsScreen from './BoardScreen.js';
import UserScreen from './UserScreen.js';
import LoginScreen from './LoginScreen.js';
import DonationModal from './DonationModal.js';
import ExportModal from './ExportModal.js';
import FieldSelectorModal from './FieldSelectorModal.js';
import Footer from './Footer.js';

const DefaultLayout = ({ component: Component, openDonationModal, ...rest }) => (
    <Box fit>
        <Container padding={ 20 } fit>
            <Box column fit>
                <Navbar openDonationModal={ openDonationModal } />
                <Box fit>
                    <Segment style={{ overflow: 'auto', flex: 1 }}>
                        <Component { ...rest } />
                    </Segment>
                </Box>
                <Footer />
            </Box>
        </Container>
    </Box>
);

const PrivateRoute = ({ component, isAuthenticated, openDonationModal, ...rest }) => (
    <Route { ...rest } render={ props => (
        isAuthenticated ?
            <DefaultLayout component={ component } openDonationModal={ openDonationModal } { ...props } /> :
            <Redirect to={{
                pathname: '/login',
                state: { from: props.location },
            }}/>
    )} />
);

// -------------------------
// Trello API
const API = {
    get({ oauth_token, endpoint, version = 1 }) {
        return fetch(
            `${process.env.API_URL}/api/v${version}/${endpoint}`,
            {
                headers: {
                    Authorization: oauth_token,
                },
            }
        );
    },

    post({ oauth_token, data, endpoint, version = 1 }) {
        return fetch(
            `${process.env.API_URL}/api/v${version}/${endpoint}`,
            {
                headers: {
                    Authorization: oauth_token,
                },
                method: 'POST',
                body: JSON.stringify(data),
            }
        );
    },
};

class App extends Component {
    constructor(props) {
        super(props);

        const oauth_token = localStorage.getItem('oauth_token');

        this.state = {
            auth: {
                oauth_token,
                loading: false,
            },
            user: {
                data: null,
                loading: false,
                error: false,
            },
            boards: {
                data: null,
                loading: false,
                error: false,
            },
            fieldSelectorModal: {
                loading: false,
                error: false,
                data: false,
            },
            exportModal: {
                error: false,
                loading: false,
                data: false,
            },
            openModal: false,
        };

        this._tokenPresent = () => {
            return !!this.state.auth.oauth_token;
        };

        const load = ({ entity, endpoint }) => () => {
            this.setState(state => ({
                ...state,
                [entity]: {
                    data: null,
                    loading: true,
                    error: false,
                },
            }));
            return (
                API.get({ oauth_token, endpoint })
                    .then(data => data.json())
                    .then(data => {
                        this.setState(state => ({
                            ...state,
                            [entity]: {
                                data,
                                loading: false,
                                error: false,
                            },
                        }));
                    })
            );
        };

        this._loadUser = load({
            entity: 'user',
            endpoint: 'user',
        });

        this._loadBoards = load({
            entity: 'boards',
            endpoint: 'boards',
        });

        this.initLoad = () => {
            this._loadUser().then(this._loadBoards);
        };

        this._getFields = boardId => {
            this.setState(state => ({
                ...state,
                fieldSelectorModal: {
                    loading: true,
                    error: false,
                    data: false,
                    board: this.state.boards.data.filter(board => board.id === boardId).pop(),
                },
                openModal: 'fieldSelectorModal',
            }));
            API.get({ oauth_token, endpoint: `boards/${boardId}/fields` })
                .then(data => data.json())
                .then(({ fields }) => {
                    const oldFields = localStorage.getItem(`board-data-${boardId}`);

                    this.setState(state => ({
                        ...state,
                        fieldSelectorModal: {
                            ...state.fieldSelectorModal,
                            open: true,
                            loading: false,
                            error: false,
                            data: {
                                fields,
                                selected: oldFields ? JSON.parse(oldFields) : fields,
                            },
                        },
                    }));
                });
        };

        this._exportBoard = (boardId, format) => {
            if (format === 'csv') {
                const fields = this.state.fieldSelectorModal.data.selected;

                localStorage.setItem(`board-data-${boardId}`, JSON.stringify(fields));

                window.open(
                    `${process.env.API_URL}/api/v1/boards/${boardId}/export/${format}?oauth_token=${oauth_token}&f=${fields.join(',')}`,
                    '_blank'
                );
                return;
            }

            this.setState(state => ({
                ...state,
                exportModal: {
                    loading: true,
                    error: false,
                    data: false,
                },
                openModal: 'exportModal',
            }));

            API.get({
                oauth_token,
                endpoint: `boards/${boardId}/export/${format}`,
            })
                .then(data => data.text())
                .then(data => {
                    switch (format) {
                        case 'markdown':
                        case 'json':
                            this.setState(state => ({
                                ...state,
                                exportModal: {
                                    ...state.exportModal,
                                    loading: false,
                                    data,
                                },
                            }));
                            break;
                        default:
                            break;
                    }
                });
        };

        this._fieldToggle = ({ field }) => {
            this.setState(state => {
                const found = state.fieldSelectorModal.data.selected.filter(f => field === f).length === 1;
                const selected = !found ?
                    [ ...state.fieldSelectorModal.data.selected, field ] :
                    state.fieldSelectorModal.data.selected.filter(f => field !== f);

                return {
                    ...state,
                    fieldSelectorModal: {
                        ...state.fieldSelectorModal,
                        data: {
                            ...state.fieldSelectorModal.data,
                            selected,
                        },
                    },
                };
            });
        };

        this._closeModal = () => {
            this.setState(state => ({
                ...state,
                openModal: false,
            }));
        };

        this._openDonationModal = () => {
            this.setState(state => ({
                ...state,
                openModal: 'donations',
            }));
        };
    }

    componentDidMount() {
        if (this._tokenPresent()) {
            this.initLoad();
        } else {
            window.addEventListener(
                'message',
                ({ data }) => {
                    const { oauth_token } = data;
                    if (oauth_token) {
                        this.setState(state => ({
                            ...state,
                            auth: {
                                oauth_token: data.oauth_token,
                                loading: false,
                            },
                        }), () => {
                            window.localStorage.setItem('oauth_token', oauth_token);
                            window.location.href = '/boards';
                            this.authPopup.close();
                            this.initLoad();
                        });
                    }
                },
                false
            );
        }
    }

    login() {
        if (this.state.auth.oauth_token) {
            window.location.href = '/boards';
            return;
        }
        this.setState(state => ({
            ...state,
            auth: {
                ...state.auth,
                loading: true,
            },
        }));

        this.authPopup = window.open(
            `${process.env.API_URL}/auth/trello`,
            '_blank',
            'location=yes,height=570,width=520,scrollbars=yes,status=yes'
        );
    }

    _closeFieldSelectorModal() {
        this.setState(state => ({
            ...state,
            fieldSelectorModal: {
                loading: false,
                error: false,
                data: false,
            },
        }), this._closeModal);
    }

    _closeExportModal() {
        this.setState(state => ({
            ...state,
            exportModal: {
                loading: false,
                error: false,
                data: false,
            },
        }), this._closeModal);
    }

    render() {
        const { auth, user, boards, exportModal, fieldSelectorModal } = this.state;

        const isAuthenticated = !!auth.oauth_token;

        return (
            <Router>
                <div style={{
                    backgroundColor: '#fafafa',
                    height: '100%',
                }}>
                    <Switch>
                        <PrivateRoute
                            path="/user"
                            isAuthenticated={ isAuthenticated }
                            openDonationModal={ this._openDonationModal }
                            component={() => (
                                <UserScreen user={ user } />
                            )}/>
                        <PrivateRoute
                            path="/boards"
                            isAuthenticated={ isAuthenticated }
                            openDonationModal={ this._openDonationModal }
                            component={() => (
                                <span>
                                    <BoardsScreen
                                        boards={ boards }
                                        onExportClick={ this._exportBoard.bind(this) }
                                        onGetFieldsClick={ this._getFields.bind(this) }
                                    />
                                    <ExportModal
                                        isOpen={ this.state.openModal === 'exportModal' }
                                        data={ exportModal }
                                        onClose={ this._closeExportModal.bind(this) }
                                    />
                                    <FieldSelectorModal
                                        isOpen={ this.state.openModal === 'fieldSelectorModal' }
                                        data={ fieldSelectorModal }
                                        onExportClick={ this._exportBoard.bind(this) }
                                        onFieldToggle={ this._fieldToggle.bind(this) }
                                        onClose={ this._closeFieldSelectorModal.bind(this) }
                                    />
                                </span>
                            )}/>
                        <Route path="/login" render={ () => (
                            <LoginScreen auth={ auth } login={ this.login.bind(this) } />
                        )} />
                        <Redirect to="/boards" />
                    </Switch>
                    <DonationModal
                        isOpen={ this.state.openModal === 'donations' }
                        onClose={ this._closeModal }
                    />
                </div>
            </Router>
        );
    }
}

export default App;
