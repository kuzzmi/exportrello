const webpack = require('webpack');
const path = require('path');
const common = require('./webpack.common.js');
const merge = require('webpack-merge');

const localhost = 'http://localhost:3001';

module.exports = merge(common, {
    devtool: 'inline-source-map',

    devServer: {
        contentBase: path.resolve(__dirname, './client/dist'),
        compress: true,
        port: 8000,
        historyApiFallback: true,
    },

    plugins: [
        new webpack.EnvironmentPlugin({
            API_URL: localhost,
        }),
    ],
});
