const express = require('express');
const app = express();

const { PORT, configureExpress } = require('./config.js');

configureExpress(app);

// -------------------------
// Authentication routes
const auth = require('./auth.js');
app.use('/auth', auth.router);
app.use(auth.attachUser);

// -------------------------
// API routes
const router = require('./router.js');
app.use('/api/v1/', router);

app.listen(PORT);
