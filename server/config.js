// Reading dotenv file
require('dotenv').config();

const {
    API_KEY,
    PORT = 3001,
    NODE_ENV = 'development',
} = process.env;

const BASE_URL =
    NODE_ENV === 'production' ?
        'https://exportrello.kuzzmi.com/api' :
        `http://localhost:${PORT}`;

const isProduction = () => NODE_ENV === 'production';

const configureExpress = app => {
    // -------------------------
    // Configuration
    const bodyParser = require('body-parser');
    const morgan = require('morgan');
    const cors = require('cors');
    const mustacheExpress = require('mustache-express');

    // Set mustache as a default renderer
    app.engine('mustache', mustacheExpress());
    app.set('view engine', 'mustache');
    app.set('views', __dirname + '/views');

    // CORS middleware
    !isProduction() && app.use(cors());

    // logging
    app.use(morgan('dev'));

    // parsing incoming request bodies
    app.use(bodyParser.json());

    // -------------------------
    // Error handling
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            error: {
                message: err.message || 'Unknown error occured',
                error: isProduction() ? err : 'Server error',
            },
        });
        next();
        throw err;
    });

};

module.exports = {
    API_KEY,
    PORT,
    NODE_ENV,
    BASE_URL,
    isProduction,
    configureExpress,
};
