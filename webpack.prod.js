const webpack = require('webpack');
const common = require('./webpack.common.js');
const merge = require('webpack-merge');

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
    plugins: [
        new webpack.EnvironmentPlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
            API_URL: '/api',
        }),
        new UglifyJSPlugin(),
    ],
});
